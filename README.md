# SC Icons

Generate icons in various colors with text for use in SC UIs.


## Requirements

* [Pillow](https://pillow.readthedocs.io/en/stable/installation/basic-installation.html)

## Run

`python icons.py <dest_directory>`
