#!/usr/local/bin/python3

import os
import sys

from pathlib import Path
from PIL import (
        Image,
        ImageDraw,
        ImageFont,
        ImageOps,
        ImageSequence,
        UnidentifiedImageError
)


def generate(base, path, im):

    if os.path.basename(path) == "_":
        name = os.path.split(os.path.dirname(path))[1]
    else:
        name = os.path.basename(path)

    draw = ImageDraw.Draw(base)
    font = ImageFont.truetype("Vermillion22-mLaO2.ttf", 35)

    draw.text((base.width/2, base.height - 10), name, fill=0, font=font,
              anchor="md", align="center")

    colors = [
        {'name': 'white', 'color': (244, 238, 244)},
        {'name': 'green', 'color': (6, 176, 21)},
        {'name': 'red', 'color': (245, 46, 67)},
    ]

    ret_images = []
    for color in colors:
        frames = []
        durations = []
        for frame in ImageSequence.Iterator(im):
            frame = frame.resize((200, 200))
            cp = base.copy()
            cp.paste(frame, (50, 30))
            frames.append(ImageOps.colorize(cp, color['color'], (0, 0, 0)))

            if 'duration' in frame.info:
                durations.append(frame.info['duration'])

        ret_images.append({
            'color': color['name'],
            'image': frames,
            'durations': durations
        })
    return ret_images


def main():
    base = Image.new("L", (300, 300))
    base = ImageOps.invert(base)

    symbols = dict()
    for root, dirs, files in os.walk('timoses'):
        root = Path(root)
        for file in files:
            file_path = root / file
            symbol_path, ext = os.path.splitext(file_path)
            if symbol_path in symbols:
                print("ERROR: Duplicate symbol '%s'" % symbol_path)
                print("    Previous: %s" % symbols[symbol_path].filename)
                print("    Current: %s" % file_path)
                continue
            try:
                im = Image.open(file_path)

                if symbol_path not in symbols:
                    symbols[symbol_path] = dict()

                symbols[symbol_path]['ext'] = ext
                symbols[symbol_path]['image'] = im
            except UnidentifiedImageError as e:
                print(e)

    results_path = Path(sys.argv[1])
    print("Storing results in %s" % results_path)
    for symbol_path, symbol in symbols.items():
        symbol_image = symbol['image']
        outputs = generate(base.copy(), symbol_path, symbol_image)
        for output in outputs:
            final_path = results_path / (symbol_path.replace('/', '_') + "_" + output['color'])
            os.makedirs(os.path.dirname(final_path), exist_ok=True)
            if len(output['image']) > 1:
                output['image'][0]. \
                    save(final_path.with_suffix(symbol['ext']),
                         save_all=True,
                         append_images=output['image'][1:],
                         duration=output['durations'])
            else:
                output['image'][0]. \
                    save(final_path.with_suffix(symbol['ext']))


if __name__ == "__main__":
    main()
